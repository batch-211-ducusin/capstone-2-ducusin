const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Retrieve User Details
router.get("/details", auth.verify, (req, res)=> {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId: userData.id})
	.then(resultFromController=> res.send(resultFromController));
});

// Check Duplicate Email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body)
	.then(resultFromController=>res.send(resultFromController));
});

// User Registration
router.post("/register", (req, res)=> {
	userController.registerUser(req.body)
	.then(resultFromController=> res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res)=> {
	userController.loginUser(req.body)
	.then(resultFromController=> res.send(resultFromController));
});

// Set User to Admin
router.put("/setToAdmin/:userId", auth.verify, (req, res)=> {
	userController.setToAdmin(req.params, req.body)
	.then(resultFromController=> res.send(resultFromController));
});

// Set Admin to User
router.put("/setToUser/:userId", auth.verify, (req, res)=> {
	userController.setToUser(req.params, req.body)
	.then(resultFromController=> res.send(resultFromController));
});

// Retrieve All Users.
router.get("/all", (req, res)=> {
	userController.getAllUsers()
	.then(resultFromController=> res.send(resultFromController));
});

//***********************************************
//Retrieve All Orders (admin only).
router.get("/orders", auth.verify, (req, res)=> {
	const admin = auth.decode(req.headers.authorization).isAdmin;
	if(admin === true) {
		userController.getAllOrders()
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});
//***********************************************

//***********************************************
//Retrieve Authenticated User's Orders (non-admin only).
router.get("/myOrders", auth.verify, (req, res)=> {
	const admin = auth.decode(req.headers.authorization).isAdmin;
	if(admin === false) {
		userController.getUserOrder()
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});
// ***********************************************

// Retrieve User Detail.
router.get("/:userId", (req, res)=> {
	userController.getUserDetail(req.params)
	.then(resultFromController=> res.send(resultFromController));
});



//Non-admin User Checkout (create order)
// Option 1
router.post("/checkout", auth.verify, (req, res)=> {
	const admin = auth.decode(req.headers.authorization).isAdmin;
	if(admin === true) {
		return false;
	} else {
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId,
			quantity: 1,
			price: req.body.price
		};
		userController.checkout(data)
		.then(resultFromController=> res.send(resultFromController));
	}
});


module.exports = router;