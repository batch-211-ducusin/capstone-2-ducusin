const express = require("express");
const productController = require("../controllers/productControllers");
const auth = require("../auth");

const router = express.Router();


//Create Product (admin Only).
router.post("/addProduct", auth.verify, (req, res)=> {
	const admin = auth.decode(req.headers.authorization).isAdmin;
	if(admin == true) {
		productController.addProduct(req.body).then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	};
});

//Retrieve All Active Product.
router.get("/active", (req, res)=> {
	productController.getActiveProduct().then(resultFromController=> res.send(resultFromController));
});

//Retrieve All Product.
router.get("/all", (req, res)=> {
	productController.getAllProduct()
	.then(resultFromController=> res.send(resultFromController));
});

//Retrieve Single Product.
router.get("/:productId", (req, res)=> {
	productController.getProduct(req.params)
	.then(resultFromController=> res.send(resultFromController));
});

// Edit Product Information (admin only).
router.put("/editProduct/:productId", (req, res)=> {
	const userAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(userAdmin == true) {
		productController.updateProduct(req.params, req.body)
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});

//Archive A Product (admin only)
// Option 1
/*router.put("/archive/:productId", auth.verify, (req, res)=> {
	productController.archiveProduct(req.params, req.body).then(resultFromController=> res.send(resultFromController));
});*/
// Option 2
router.put("/archive/:productId", auth.verify, (req, res)=> {
	const userAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(userAdmin) {
		productController.archiveProduct(req.params, req.body)
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});


//Activate Product (admin only)
router.put("/activate/:productId", auth.verify, (req, res)=> {
	const userAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(userAdmin) {
		productController.activateProduct(req.params, req.body)
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});

//Retrieve Authenticated User's Order (Non-admin only).
router.post("/orders", auth.verify, (req, res)=> {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	if(isAdmin === true) {
		userController.getAllOrder({ userId: req.body.userId })
		.then(resultFromController=> res.send(resultFromController));
	} else {
		return false;
	}
});


module.exports = router;