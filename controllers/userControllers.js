const User = require("../models/User");
const Product = require("../models/Product");
const auth = require("../auth");
const bcrypt = require("bcrypt");

// Retrive User Details
module.exports.getProfile = (data)=> {
	return User.findById(data.userId)
	.then(result=> {
		result.password = "";
		return result;
	})
};

// Check Duplicate Email
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email})
	.then(result=>{
		if(result.length>0){
			return true;
		}else{
			return false;
		};
	});
};

//User Registration
module.exports.registerUser = (reqBody)=> {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
		// bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save()
	.then((user, error)=> {
		if(error) {
			return false;
		} else {
			return User.find(reqBody.userId)
			.then(result=> {return result;});
		}
	});
};


//User Authentication
module.exports.loginUser = (reqBody)=> {
	return User.findOne({email: reqBody.email})
	.then(result=> {
		if(result === null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	});
};

// Set User to Admin
module.exports.setToAdmin = (reqParams, reqBody)=> {
	let adminStatus = {isAdmin: true};
	return User.findByIdAndUpdate(reqParams.userId, adminStatus)
	.then((user, error)=> {
		if(error) {
			return false;
		} else {
			return User.findById(reqParams.userId)
			.then(result=> {return result;});
		}
	});
};

// Set Admin to User
module.exports.setToUser = (reqParams, reqBody)=> {
	let adminStatus = {isAdmin: false};
	return User.findByIdAndUpdate(reqParams.userId, adminStatus)
	.then((user, error)=> {
		if(error) {
			return false;
		} else {
			return User.findById(reqParams.userId)
			.then(result=> {return result;});
		}
	});
};

//Retrieve All Users.
module.exports.getAllUsers = ()=> {
	return User.find({}).then(result=> {return result;});
};

//***********************************************
//Retrieve All Orders (admin only).
module.exports.getAllOrders = (reqParams)=> {
	return User.findOne(reqParams, {orders: 1})
	.then(result=> {return result;});
};
//***********************************************

//***********************************************
// Retrieve Authenticated User's Orders (non-admin only).
module.exports.getUserOrder = (reqParams)=> {
	return User.findOne(reqParams, {orders: 1})
	.then(result=> {return result});
};
//***********************************************

//Retrieve User Detail.
module.exports.getUserDetail = (reqParams)=> {
	return User.findById(reqParams.userId)
	.then(result=> {return result});
};

//Non-admin User Checkout (create order)
// Option 1
module.exports.checkout = async (data)=> {
	let userOrder = await User.findById(data.userId)
	.then(user=> {user.orders.push({
		productId: data.productId,
		quantity: data.quantity,
		price: data.price
	});
		return user.save()
		.then((user, error)=> {
			if(error) {
				return false;
			} else {
				return true;
			};
		});
	});
	
	// Dec 5 additional, sinusubukan kong ipalabas ang price.
	// let price = await Product.findById(data.productId)
	// .then(result=> {return result.price});

	let productOrder = await Product.findById(data.productId)
	.then(product=> {product.orders.push({userId: data.userId});
		return product.save()
		.then((product, error)=> {
			if(error) {
				return false;
			} else {
				return true; 
			}
		});
	});
	if(userOrder && productOrder) {
		return true;
	} else {
		return false;
	};
};


//Retrieve All Orders (admin only).
// Option 1
/*module.exports.getAllOrder = (reqParams)=> {
	return User.findById(reqParams.userId, {orders: 1}).then(result=> {return result;});
};*/
// Option 2
/*module.exports.getAllOrders = ()=> {
	return User.find({}).then(result=> {return result;});
};
*/
//