const Product = require("../models/Product");
const auth = require("../auth");


//Create Product (Admin Only).
module.exports.addProduct = (reqBody)=> {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	return newProduct.save().then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.find({}).then(result=> {return result});
		}
	});
};

//Retrieve All Active Product.
module.exports.getActiveProduct = ()=> {
	return Product.find({isActive: true})
	.then(result=> {
		return result;
	});
};

//Retrieve All Product.
module.exports.getAllProduct = ()=> {
	return Product.find({})
	.then(result=> {
		return result;
	});
};

//Retrieve Single Product.
module.exports.getProduct = (reqParams)=> {
	return Product.findById(reqParams.productId)
	.then(result=> {return result});
};

// Edit Product Information (admin only).
module.exports.updateProduct = (reqParams, reqBody)=> {
	const updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.findById(reqParams.productId)
			.then(result=> {
				return result;
			});
		}
	});
};

//Archive A Product (admin only)
module.exports.archiveProduct = (reqParams, reqBody)=> {
	let productStatus = {isActive: reqBody.isActive};
	return Product.findByIdAndUpdate(reqParams.productId, productStatus).then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.findById(reqParams.productId)
			.then(result=> {
				return result;
			});
		}
	});
};

//Activate Product (admin only)
module.exports.activateProduct = (reqParams, reqBody)=> {
	let activeStatus = {isActive: true};
	return Product.findByIdAndUpdate(reqParams.productId, activeStatus).then((product, error)=> {
		if(error) {
			return false;
		} else {
			return Product.findById(reqParams.productId)
			.then(result=> {return result;});
		}
	});
};

//Retrieve Authenticated User's Order (Non-admin only).
module.exports.getAllOrder = (reqParams)=> {
	return User.findById(reqParams.userId, {orders: 1}).then(result=> {return result;});
};