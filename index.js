const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const productRoutes = require("./routes/productRoutes");
const userRoutes = require("./routes/userRoutes");
const dotenv = require("dotenv");

const app = express();

dotenv.config();
mongoose.connect(
	process.env.MONGO_URL,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once('open', ()=> console.log("Now connected to Mongodb Atlas."));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, ()=> {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});